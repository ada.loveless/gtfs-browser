use serde::{Deserialize, Serialize};
use serde_repr::{Deserialize_repr, Serialize_repr};
use serde_with::{serde_as, BoolFromInt};

use chrono::NaiveDate;

use std::{fmt::Display, fs::File};
use zip::read::ZipArchive;

#[derive(Debug)]
pub struct Time {
    pub h: u64,
    pub m: u64,
    pub s: u64,
}

impl Time {
    pub fn new(h: u64, m: u64, s: u64) -> Self {
        Time { h, m, s }
    }
}

impl Serialize for Time {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let s = format!("{}:{}:{}", self.h, self.m, self.s);
        serializer.serialize_str(&s)
    }
}

impl<'de> Deserialize<'de> for Time {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        let s = String::deserialize(deserializer)?;
        let s: Vec<&str> = s.split(':').collect();
        match s.len() {
            3 => {
                let h = s[0].parse().map_err(serde::de::Error::custom)?;
                let m = s[1].parse().map_err(serde::de::Error::custom)?;
                let s = s[2].parse().map_err(serde::de::Error::custom)?;
                Ok(Time { h, m, s })
            }
            _ => Err(serde::de::Error::custom("Malformatted time")),
        }
    }
}

impl Display for Time {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Time({}:{}:{})", self.h, self.m, self.s)
    }
}

mod date {
    use chrono::NaiveDate;
    use serde::{self, Deserialize, Deserializer, Serializer};

    const FORMAT: &str = "%Y%m%d";

    pub fn serialize<S>(date: &NaiveDate, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let s = format!("{}", date.format(FORMAT));
        serializer.serialize_str(&s)
    }

    pub fn deserialize<'de, D>(deserializer: D) -> Result<NaiveDate, D::Error>
    where
        D: Deserializer<'de>,
    {
        let s = String::deserialize(deserializer)?;
        let dt = NaiveDate::parse_from_str(&s, FORMAT).map_err(serde::de::Error::custom)?;
        Ok(dt)
    }
}

mod opt_date {
    use chrono::NaiveDate;
    use serde::{self, Deserialize, Deserializer, Serializer};

    const FORMAT: &str = "%Y%m%d";

    pub fn serialize<S>(dt: &Option<NaiveDate>, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        match dt {
            Some(dt) => serializer.serialize_str(&format!("{}", dt.format(FORMAT))),
            None => serializer.serialize_str(""),
        }
    }

    pub fn deserialize<'de, D>(deserializer: D) -> Result<Option<NaiveDate>, D::Error>
    where
        D: Deserializer<'de>,
    {
        let s: Option<String> = Option::deserialize(deserializer)?;
        match s {
            Some(s) => Ok(Some(
                NaiveDate::parse_from_str(&s, FORMAT).map_err(serde::de::Error::custom)?,
            )),
            None => Ok(None),
        }
    }
}

pub trait GtfsObject<'a>: Deserialize<'a> {
    const FILE: &'static str;
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Agency {
    pub agency_id: Option<String>,
    pub agency_name: String,
    pub agency_url: String,
    pub agency_timezone: String,
    pub agency_lang: Option<String>,
    pub agency_phone: Option<String>,
    pub agency_fare_url: Option<String>,
    pub agency_email: Option<String>,
}
impl<'a> GtfsObject<'a> for Agency {
    const FILE: &'static str = "agency.txt";
}

#[derive(Debug, Serialize_repr, Deserialize_repr)]
#[repr(u8)]
pub enum LocationType {
    Stop = 0,
    Station = 1,
    EntranceExit = 2,
    GenericNode = 3,
    BoardingArea = 4,
}

#[derive(Debug, Serialize_repr, Deserialize_repr)]
#[repr(u8)]
pub enum WheelchairAccessibility {
    // NB: These depend on other fields in the stop field and are a bit of a mess. Please consider
    // them to be -ish
    Unknown = 0,
    Yes = 1,
    No = 2,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Stop {
    pub stop_id: String,
    pub stop_code: Option<String>,
    pub stop_name: Option<String>,
    pub tts_stop_name: Option<String>,
    pub stop_desc: Option<String>,
    pub stop_lat: Option<f64>,
    pub stop_lon: Option<f64>,
    pub zone_id: Option<String>,
    pub stop_url: Option<String>,
    pub location_type: Option<LocationType>,
    pub parent_station: Option<String>,
    pub stop_timezone: Option<String>,
    pub wheelchair_boarding: Option<WheelchairAccessibility>,
    pub level_id: Option<String>,
    pub platform_code: Option<String>,
}
impl<'a> GtfsObject<'a> for Stop {
    const FILE: &'static str = "stops.txt";
}

#[derive(Debug, Serialize_repr, Deserialize_repr)]
#[repr(u8)]
pub enum RouteType {
    Tram = 0,
    Metro = 1,
    Rail = 2,
    Bus = 3,
    Ferry = 4,
    CableTram = 5,
    AerialLift = 6,
    Funicular = 7,
    Trolleybus = 11,
    Monorail = 12,
}

#[derive(Debug, Serialize_repr, Deserialize_repr)]
#[repr(u8)]
pub enum PickupType {
    RegularSchedule = 0,
    NoPickup = 1,
    PhoneAgency = 2,
    CoordinateWithDriver = 3,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Route {
    pub route_id: String,
    pub agency_id: Option<String>,
    pub route_short_name: Option<String>,
    pub route_long_name: Option<String>,
    pub route_desc: Option<String>,
    pub route_type: Option<RouteType>,
    pub route_url: Option<String>,
    pub route_color: Option<String>,
    pub route_text_color: Option<String>,
    pub route_sort_order: Option<u64>,
    pub continuous_pickup: Option<PickupType>,
    pub continuous_drop_off: Option<PickupType>,
    pub network_id: Option<String>,
}
impl<'a> GtfsObject<'a> for Route {
    const FILE: &'static str = "routes.txt";
}

#[derive(Debug, Serialize_repr, Deserialize_repr)]
#[repr(u8)]
pub enum Direction {
    Outbound = 0,
    Inbound = 1,
}

#[derive(Debug, Serialize_repr, Deserialize_repr)]
#[repr(u8)]
pub enum BikesAllowed {
    Unknown = 0,
    Yes = 1,
    No = 2,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Trip {
    pub route_id: String,
    pub service_id: String,
    pub trip_id: String,
    pub trip_headsign: Option<String>,
    pub trip_short_name: Option<String>,
    pub direction_id: Option<Direction>,
    pub block_id: Option<String>,
    pub shape_id: Option<String>,
    pub wheelchair_accessible: Option<WheelchairAccessibility>,
    pub bikes_allowed: Option<BikesAllowed>,
}
impl<'a> GtfsObject<'a> for Trip {
    const FILE: &'static str = "trips.txt";
}

#[derive(Debug, Serialize_repr, Deserialize_repr)]
#[repr(u8)]
pub enum TimepointType {
    Approximate = 0,
    Exact = 1,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct StopTime {
    pub trip: Option<String>,
    pub arrival_time: Option<Time>,
    pub departure_time: Option<Time>,
    pub stop_id: Option<String>,
    pub stop_sequence: Option<u64>,
    pub stop_headsign: Option<String>,
    pub pickup_type: Option<PickupType>,
    pub drop_off_type: Option<PickupType>,
    pub continuous_pickup: Option<PickupType>,
    pub contiuous_drop_off: Option<PickupType>,
    pub shape_dist_travelled: Option<f64>,
    pub timepoint: Option<TimepointType>,
}
impl<'a> GtfsObject<'a> for StopTime {
    const FILE: &'static str = "stop_times.txt";
}

#[serde_as]
#[derive(Debug, Deserialize, Serialize)]
pub struct Calendar {
    pub service_id: String,
    #[serde_as(as = "BoolFromInt")]
    pub monday: bool,
    #[serde_as(as = "BoolFromInt")]
    pub tuesday: bool,
    #[serde_as(as = "BoolFromInt")]
    pub wednesday: bool,
    #[serde_as(as = "BoolFromInt")]
    pub thursday: bool,
    #[serde_as(as = "BoolFromInt")]
    pub friday: bool,
    #[serde_as(as = "BoolFromInt")]
    pub saturday: bool,
    #[serde_as(as = "BoolFromInt")]
    pub sunday: bool,
    #[serde(with = "date")]
    pub start_date: NaiveDate,
    #[serde(with = "date")]
    pub end_date: NaiveDate,
}
impl<'a> GtfsObject<'a> for Calendar {
    const FILE: &'static str = "calendar.txt";
}

#[derive(Debug, Serialize_repr, Deserialize_repr)]
#[repr(u8)]
pub enum CalendarException {
    Added = 1,
    Removed = 2,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct CalendarDates {
    pub service_id: String,
    #[serde(with = "date")]
    pub date: NaiveDate,
    pub exception_type: CalendarException,
}
impl<'a> GtfsObject<'a> for CalendarDates {
    const FILE: &'static str = "calendar_dates.txt";
}

#[derive(Debug, Serialize_repr, Deserialize_repr)]
#[repr(u8)]
pub enum TransferCount {
    None = 0,
    One = 1,
    Two = 2,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct FareAttributes {
    pub fare_id: String,
    pub price: f64,
    pub currency_type: String,
    pub payment_method: u64,
    // While transfers can be nulled, that does not mean nothing it implies unlimited transfers.
    pub transfers: Option<TransferCount>,
    pub agency_id: Option<String>,
    pub transfer_duration: Option<u64>,
}
impl<'a> GtfsObject<'a> for FareAttributes {
    const FILE: &'static str = "fare_attributes.txt";
}

#[derive(Debug, Deserialize, Serialize)]
pub struct FareRules {
    pub fare_id: String,
    pub route_id: Option<String>,
    pub origin_id: Option<String>,
    pub destination_id: Option<String>,
    pub contains_id: Option<String>,
}
impl<'a> GtfsObject<'a> for FareRules {
    const FILE: &'static str = "fare_rules.txt";
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Timeframes {
    pub timeframe_group_id: String,
    pub start_time: Option<Time>,
    pub end_time: Option<Time>,
    pub service_id: String,
}
impl<'a> GtfsObject<'a> for Timeframes {
    const FILE: &'static str = "timeframes.txt";
}

#[derive(Debug, Serialize_repr, Deserialize_repr)]
#[repr(u8)]
pub enum FareMediaType {
    None = 0,
    Paper = 1,
    Card = 2,
    Cemv = 3,
    App = 4,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct FareMedia {
    pub fare_media_id: String,
    pub fare_media_name: Option<String>,
    pub fare_media_type: FareMediaType,
}
impl<'a> GtfsObject<'a> for FareMedia {
    const FILE: &'static str = "fare_media.txt";
}

#[derive(Debug, Deserialize, Serialize)]
pub struct FareProducts {
    pub fare_product_id: String,
    pub fare_product_name: Option<String>,
    pub fare_media_id: Option<String>,
    pub amount: f64,
    pub currency: String,
}
impl<'a> GtfsObject<'a> for FareProducts {
    const FILE: &'static str = "fare_products.txt";
}

#[derive(Debug, Deserialize, Serialize)]
pub struct FareLegRules {
    pub leg_group_id: Option<String>,
    pub network_id: Option<String>,
    pub from_area_id: Option<String>,
    pub to_area_id: Option<String>,
    pub from_timeframe_group_id: Option<String>,
    pub to_timeframe_group_id: Option<String>,
    pub fare_product_id: String,
}
impl<'a> GtfsObject<'a> for FareLegRules {
    const FILE: &'static str = "fare_leg_rules.txt";
}

#[derive(Debug, Serialize_repr, Deserialize_repr)]
#[repr(u8)]
pub enum DurationLimitType {
    DepartureAndArrival = 0,
    DepartureAndDeparture = 1,
    ArrivalAndDeparture = 2,
    ArrivalAndArrival = 3,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct FareTransferRules {
    pub from_leg_group_id: Option<String>,
    pub to_leg_group_id: Option<String>,
    pub transfer_count: Option<i64>,
    pub duration_limit: Option<u64>,
    pub duration_limit_type: Option<DurationLimitType>,
    pub fare_transfer_type: u64, // What the actual fuck is this
    pub fare_product_id: Option<String>,
}
impl<'a> GtfsObject<'a> for FareTransferRules {
    const FILE: &'static str = "fare_transfer_rules.txt";
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Areas {
    pub area_id: String,
    pub area_name: Option<String>,
}
impl<'a> GtfsObject<'a> for Areas {
    const FILE: &'static str = "areas.txt";
}

#[derive(Debug, Deserialize, Serialize)]
pub struct StopAreas {
    pub area_id: String,
    pub stop_id: String,
}
impl<'a> GtfsObject<'a> for StopAreas {
    const FILE: &'static str = "stop_areas.txt";
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Networks {
    pub network_id: String,
    pub network_name: Option<String>,
}
impl<'a> GtfsObject<'a> for Networks {
    const FILE: &'static str = "networks.txt";
}

#[derive(Debug, Deserialize, Serialize)]
pub struct RouteNetworks {
    pub network_id: String,
    pub route_id: String,
}
impl<'a> GtfsObject<'a> for RouteNetworks {
    const FILE: &'static str = "route_networks.txt";
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Shapes {
    pub shape_id: String,
    pub shape_pt_lat: f64,
    pub shape_pt_lon: f64,
    pub shape_pt_sequence: u64,
    pub shape_dist_travelled: f64,
}
impl<'a> GtfsObject<'a> for Shapes {
    const FILE: &'static str = "shapes.txt";
}

#[serde_as]
#[derive(Debug, Deserialize, Serialize)]
pub struct Frequencies {
    pub trip_id: String,
    pub start_time: Time,
    pub end_time: Time,
    pub headway_secs: u64,
    #[serde_as(as = "Option<BoolFromInt>")]
    pub exact_times: Option<bool>,
}
impl<'a> GtfsObject<'a> for Frequencies {
    const FILE: &'static str = "frequencies.txt";
}

#[derive(Debug, Serialize_repr, Deserialize_repr)]
#[repr(u8)]
pub enum TransferType {
    Recommended = 0,
    Timed = 1,
    MinTime = 2,
    NotPossible = 3,
    InSeat = 4,
    Reboard = 5,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Transfers {
    pub from_stop_id: Option<String>,
    pub to_stop_id: Option<String>,
    pub from_route_id: Option<String>,
    pub to_route_id: Option<String>,
    pub from_trip_id: Option<String>,
    pub to_trip_id: Option<String>,
    pub transfer_type: TransferType,
    pub min_transfer_time: Option<u64>,
}
impl<'a> GtfsObject<'a> for Transfers {
    const FILE: &'static str = "transfers.txt";
}

#[derive(Debug, Serialize_repr, Deserialize_repr)]
#[repr(u8)]
pub enum PathwayMode {
    Walkway = 1,
    Stairs = 2,
    MovingSidewalk = 3,
    Escalator = 4,
    Elevator = 5,
    FareGate = 6,
    ExitGate = 7,
}

#[serde_as]
#[derive(Debug, Deserialize, Serialize)]
pub struct Pathways {
    pub pathway_id: String,
    pub from_stop_id: String,
    pub to_stop_id: String,
    pub pathway_mode: PathwayMode,
    #[serde_as(as = "BoolFromInt")]
    pub is_bidirectional: bool,
    pub length: Option<f64>,
    pub traversal_time: Option<u64>,
    pub stair_count: Option<i64>,
    pub max_slope: Option<f64>,
    pub min_width: Option<f64>,
    pub signposted_as: Option<String>,
    pub reversed_signposted_as: Option<String>,
}
impl<'a> GtfsObject<'a> for Pathways {
    const FILE: &'static str = "pathways.txt";
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Levels {
    pub level_id: String,
    pub level_index: f64,
    pub level_name: Option<String>,
}
impl<'a> GtfsObject<'a> for Levels {
    const FILE: &'static str = "levels.txt";
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Translations {
    // This enum is confusingly documented:
    pub table_name: String,
    pub field_name: String,
    pub language: String,
    pub translation: String,
    pub record_id: Option<String>,
    pub record_sub_id: Option<String>,
    pub field_value: Option<String>,
}
impl<'a> GtfsObject<'a> for Translations {
    const FILE: &'static str = "Translations.txt";
}

#[derive(Debug, Deserialize, Serialize)]
pub struct FeedInfo {
    pub feed_publisher_name: String,
    pub feed_publisher_url: String,
    pub feed_lang: String,
    pub default_lang: Option<String>,
    #[serde(default)]
    #[serde(with = "opt_date")]
    pub feed_start_date: Option<NaiveDate>,
    #[serde(default)]
    #[serde(with = "opt_date")]
    pub feed_end_date: Option<NaiveDate>,
    pub feed_version: Option<String>,
    pub feed_contact_email: Option<String>,
    pub feed_contact_url: Option<String>,
}
impl<'a> GtfsObject<'a> for FeedInfo {
    const FILE: &'static str = "feed_info.txt";
}

#[serde_as]
#[derive(Debug, Deserialize, Serialize)]
pub struct Attributions {
    pub attribution_id: Option<String>,
    pub agency_id: Option<String>,
    pub route_id: Option<String>,
    pub trip_id: Option<String>,
    pub organization_name: String,
    #[serde_as(as = "Option<BoolFromInt>")]
    pub is_producer: Option<bool>,
    #[serde_as(as = "Option<BoolFromInt>")]
    pub is_operator: Option<bool>,
    #[serde_as(as = "Option<BoolFromInt>")]
    pub is_authority: Option<bool>,
    pub attribution_url: Option<String>,
    pub attribution_email: Option<String>,
    pub attribution_phone: Option<String>,
}
impl<'a> GtfsObject<'a> for Attributions {
    const FILE: &'static str = "attributions.txt";
}

#[derive(Debug)]
pub struct GtfsFile {
    archive: ZipArchive<File>,
}

pub type Iter<'a, T> = csv::DeserializeRecordsIntoIter<zip::read::ZipFile<'a>, T>;

impl GtfsFile {
    pub fn new(filename: &String) -> Self {
        let reader = std::fs::File::open(filename).unwrap();
        let archive = ZipArchive::new(reader).unwrap();
        GtfsFile { archive }
    }

    pub fn read_vec<T>(&mut self) -> Vec<T>
    where
        T: for<'a> GtfsObject<'a>,
    {
        let mut out = vec![];
        for result in self.iter() {
            let record: T = result.unwrap();
            out.push(record);
        }
        out
    }

    pub fn iter<T>(&mut self) -> Iter<T>
    where
        T: for<'a> GtfsObject<'a>,
    {
        let file = self.archive.by_name(T::FILE).unwrap();
        let reader = csv::Reader::from_reader(file);
        reader.into_deserialize::<T>()
    }
}
