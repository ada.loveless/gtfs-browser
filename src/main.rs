mod gtfs;

use clap::{Parser, Subcommand};

#[derive(Debug, Subcommand)]
enum Command {
    /// Command to input data
    Input { filename: String },
}

#[derive(Parser)]
struct App {
    /// Command to run
    #[clap(subcommand)]
    command: Command,
}

impl App {
    fn run(&self) {
        match &self.command {
            Command::Input { filename } => {
                let mut in_file = gtfs::GtfsFile::new(filename);
                let agencies: gtfs::Iter<gtfs::FeedInfo> = in_file.iter();
                for agency in agencies {
                    println!("{:?}", agency.unwrap());
                }
            }
        }
    }
}

fn main() {
    env_logger::init();
    let app = App::parse();
    app.run();
}
